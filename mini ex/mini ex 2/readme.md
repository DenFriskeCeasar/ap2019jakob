![Screenshot](dsf.PNG)

[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%202/index.html)

My project is called: ”Responding to the Impossible with an Emoji”.

Our task was to create two emojis, this project has a possibility for around 11.000 different ones. The biggest inspiration for the project was Multi by David Reinfurt, but I 
found his project too boring, too simple. I found working with shapes and simple symbols too monotone as well. I wanted it to be an emoji generator consisting of the same art 
style as Apples official emojis. So I downloaded around 30 different emojis, chopped out the different parts of the emojis in photoshop and added them to my code. The program 
chooses a different left eye, a different right eye and a different mouth every time you press the mouse (and if you hold you mouse pressed you will see the program flickering
between every emoji). I used the random() command to get this effect. More specifically I added all the left eyes in to a group, all the right in a different group and the mouths
in the last group and let the program choose at random which one it would show when you clicked the mouse.

In "Modifying the Universal" there is a lot of talk about how the emoji has evolved into something, which is no longer just a simple reaction to something, but it has become 
something that people felt has to represent your identity and not just an emotion. I dislike how it has become this way and wanted to use the emoji as a representation of a feeling. My program represents this desire. It only uses the yellow emoji face and all the possible generated emojis are yellow faces. But in which situation could you find yourself in, where you could only respond with an emotion, but you did not know which emoji to use. 
The assignment stated that we had to work with the shape related syntax, which I had not done at this point. So I wondered for a bit how you could integrate shapes and colors.
In the end I created what looked like a facebook chat using the rect(), ellipse(), text() and img() syntaxes. This chat should represent a situation where you had a strong
emotion, which would be very hard to respond to both using words and emojis. This is a social situation where almost every possible 11.000 emojis could correspond to a possible
response. My goal of using emojis to only express feeling was met.
