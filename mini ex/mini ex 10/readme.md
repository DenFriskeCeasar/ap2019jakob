**Idividual work**

I have chosen to look at the most complex individual program i have written, "A Steroid Asteroid". This flowchart is made from the users perspective and not precisely what the program does. I could have chosen to focus on the technical aspects of the program and the flowchart would almost be the same, but also a little bit different.

![Screenshot](Untitled Diagram.png)

[Link to the program](https://gitlab.com/DenFriskeCeasar/ap2019jakob/tree/master/mini%20ex/mini%20ex%206)



**Group Work**

As a group we brainstormed over ideas of power, temporality, life and much more and came up with two ideas:

**Remié Serché**

![Screenshot](Møller Remie.png)

This is a resource management game where you control a mill, which is of emblematic of economic monopoly and the power that comes with it. In the game you must constantly improve and increase profit or risk too many angry customers which could lead to game over. Below we have made a flowchart that depicts the main interactions in the game in order to give a sense of how the game would be played.

This flowchart does not differ to greatly from my individual flowchart as it is on a similar level of abstraction: it depicts more the user interaction and feedback from the program than the detailed operations of the program. It even has a similar number of input from the user although this one is more complex than the mini_ex8. We also used a different flowchart program that allowed for real time cooperation in making the flowchart which meant that we used some different shapes than my individual flowchart.
We have identified the following possible technical challenges with this program:

Difficulty with creating enough obvious conceptual linkage to the themes of the course.
Bug fixing might take a lot of time with such a complicated program.
Requires a lot of graphical elements and possibly animation, we do not want to rely to heavily on text.
Balancing the gameplay. Should give the player a sense of progression but also increased difficulty.


**Mono no Aware**

![Screenshot](Flower.png)

This program attempts to turn your browser window into a terrarium for a virtual cherry tree. The tree requires actual real-world light to grow as well as space (pixels on your screen) and virtual water (a button you must press at regular intervals). The idea is to have this program be a very slow experience and that this virtual life-form appropriates your screen -time and -space possibly even forcing you to turn your computer towards a window in order to give the tree enough light to grow, thus rendering your computer useless for you. Below is a flowchart of the life-loop of the virtual plant which determines wether it will grow wither or simply stay the same.

This flowchart is taxonomically quite different from my individual flowchart as it is meant to represent the logic flow of the computer at a lower level than my individual flowchart. It again has a slightly different visual style mostly due to the fact that we worked together on it and used a different editor.
We have identified the following possible technical challenges with this program:

We want to make a very slow experience but that is difficult to present in a reasonable time for an exam.
Generative art can be technically difficult or “mathy” to get right. And all of the plants expression has to come through the image that is generated.
The light detection library might be tricky, it plays as an essential part in the functionality of the program, but given that we have never worked with this type of library before, we could end up with having to use a suboptimal alternative.


Algorithms beyond flowcharts
Algorithms represents a very high-ground ground approach to problem solving. It is at its heart an attempt to solve known and clearly defined problems. This is think is indicative of a greater cultural movement which has been underway throughout the past century that sees the world as calculable and thus all problems are solvable without error from uncertainty if enough is known about the problem before hand. In the text: The Multiplicity of Algorithms Taina Bucher argues, "algorithms do not merely have power and politics; they are fundamentally productive of new ways of ordering the world. Importantly, algorithms do not work on their own but need to be understood as part of a much wider network of relations and practices" (2018, emphasis from the original). By acknowledging the productive acting of algorithms in the world that Bucher brings light to and the underlying assumptions behind the production and deployment of algorithms, we must be weary of the biases they perpetuate and enforce and seek to answer the question: who/what is accountable for the actions of an algorithm?