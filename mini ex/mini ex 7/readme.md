![Screenshot](poo.PNG)

[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%207/index.html)

This program was made in collaboration Thomas Farver and Maja Størum and is called “wall drawing #118^2”
As in can be deciphered from the program title, the program is heavily inspired by “wall drawing #118” by Sol Lewitt. The first rule of the program is also inspired by the
rule of his wall-drawing: “Fifty randomly placed points all connected by straight lines.”, our rule was a little different as the program was too laggy with 50 points, so 
instead the rule is:

1.	Create 40 points an have them all connected with lines.

With the first rule in place, we wanted all the points to move in different directions and in different speed, so theprogram would seem more alive, when you load it.
The second rule became:

2.	Have all the points move in different directions and speeds

To not have all the points just move out of the frame a third rule was made:

3.	Have the points bounce of the wall with the same speed, but have the negative exit angle

With these three rules in place and coded, we were sort of happy with the program, but we wanted to experiment with strokeWeight and have the lines be just at little bit wider,
the closer they were to another point. This was done out of a desire to make the program seem more 3 dimensional, even thought it is no made with the WEBGL syntax. This is our 
fourth rule:

4.	Have the lines become gradually closer to double width, when they come closer to another point

The last rule we made is:

5.	Have the points disappear, when they are within 30 pixels of anther point and reappear when they are further than 100 pixels away.

This final rule met our desire of making the program seem 3 dimensional and completely random. In his sentence the keyword is “seem”, because the code is not completely random,
more “pseudo-random”. When you have the knowledge of what rules the program is based on, you can see it in action and maybe realize that it is not completely random. 
