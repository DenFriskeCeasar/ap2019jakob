
var bullets;
var asteroids;
var ship;
var shipImage, bulletImage;

var MARGIN = 40;

function setup() {
  createCanvas(800, 600);

  bulletImage = loadImage('water.png');
  shipImage = loadImage('syringe.png');
  arm = loadImage('strong arm.png');



  ship = createSprite(width/2, height/2);
  ship.scale = 0.1
  ship.friction = 0.98;
  ship.setCollider('circle', 0, 0, 20);
  ship.addImage(shipImage);


  asteroids = new Group();
  bullets = new Group();

  for(var i = 0; i<8; i++) {
    var ang = random(360);
    var px = width/2 + 1000 * cos(radians(ang));
    var py = height/2+ 1000 * sin(radians(ang));
    createAsteroid(4, px, py);
  }
}

function draw() {
  background(0);

  fill(255);
  textAlign(CENTER);
  text('pump up those arms with arrow keys and x', width/2, 20);

  for(var i=0; i<allSprites.length; i++) {
    var s = allSprites[i];
    if(s.position.x<-MARGIN) s.position.x = width+MARGIN;
    if(s.position.x>width+MARGIN) s.position.x = -MARGIN;
    if(s.position.y<-MARGIN) s.position.y = height+MARGIN;
    if(s.position.y>height+MARGIN) s.position.y = -MARGIN;
  }

  asteroids.overlap(bullets, asteroidHit);

  ship.bounce(asteroids);

  if(keyDown(LEFT_ARROW))
    ship.rotation -= 4;
  if(keyDown(RIGHT_ARROW))
    ship.rotation += 4;


  if(keyWentDown('x'))
  {
    var bullet = createSprite(ship.position.x, ship.position.y);
    bullet.addImage(bulletImage);
    bullet.scale = 0.02
    bullet.setSpeed(10+ship.getSpeed(), ship.rotation+155);
    bullet.life = 30;
    bullets.add(bullet);
  }

  drawSprites();

}

function createAsteroid(type, x, y) {
  var a = createSprite(x, y);
  a.addImage(arm);
  a.setSpeed(2.5-(type/2), random(360));
  a.rotationSpeed = 0.5;
  a.scale = 0.05

  a.type = type;

  if(type == 3)
    a.scale = 0.1;

  if(type == 2)
    a.scale = 0.15;

  if(type == 1)
    a.scale = 0.2;


  a.mass = 2+a.scale;
  a.setCollider('circle', 0, 0, 50);
  asteroids.add(a);
  return a;
}

function asteroidHit(asteroid, bullet) {
  var newType = asteroid.type-1;

  if(newType>0) {
    createAsteroid(newType, asteroid.position.x, asteroid.position.y);


  }

  bullet.remove();
  asteroid.remove();
}
