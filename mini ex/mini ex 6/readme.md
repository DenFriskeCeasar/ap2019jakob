![Screenshot](Udklip.PNG)

[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%206/index.html)

My game is called “A Steroid Asteroid” 

When you start the game you see a syringe and arms floating around. You also see a text which reads: “pump up those arms with arrow keys and x”, this should guide you to use
these functions. When you do, you will realize that the arrow keys moves the syringe clockwise and counter-clockwise and x makes the syringe shoot out “water”. When this “water” 
hits the arm the arm grows bigger and faster until you have hit it 3 times, then it disappears. There is no end to the game and you cannot lose. The only goal of the game is to
make all of the arms disappear. 

The concept of the game was created based on the pun in its name and my wish to create my own code using existing code. The game is based on the classic asteroid shooter game. 

There are 3 main objects in this game(syringe, arm and “water”) and they are all created with the createSprite function in p5.play. The most complicated to code of these are 
the arms. They have to rotate, bounce around, leave and enter the frame, change size and speed when hit by the “water” and disappear when hit enough times. Originally I wanted 
it to change picture as well, but this was too complicated for me.

Object-oriented programming makes it a lot easier to create functions, who relate naturally to each other. This also makes it easier to conceptualize the game before coding,
because the functions interacts as if they would in real life. 

I have a hard time trying to describe my game project to wider digital culture context, but when you know from real life that steroids make arms bigger, this should help you 
understand the function of the syringe in my game. I guess. 

I don’t really have more to say. This game is stupid.
