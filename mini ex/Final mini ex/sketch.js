
var backgroundcolor
var

function setup(){

createCanvas (windowWidth,windowHeight);
noStroke();
colorMode(HSB, 100, 100, 100);

}

function draw() {
  rectMode(CORNER); // Default rectMode is CORNER
  fill(255); // Set fill to white
  rect(25, 25, 50, 50); // Draw white rect using CORNER mode
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);

}
