![Screenshot](sfs.png)

[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%201/index.html)

The webpage is an art illistration i have chosen to name: "The Capitalismic Influence of Children in a Boring Dystopia" 

Starting to work with p5.js was very difficult as I have no former experience with coding or programing. But after decoding what the basic coding language meant, it quickly became 
just editing values and copy pasting other people’s codes. My interest in the beginning was not great as just using “ellipse()” to make circles was boring, I had no drive to make
something. The moment I started having fun with p5.js was when I learned how to add images. The image you see in the final link was also the first image I implemented. It was just 
a random funny image I had on my computer. Now I had a drive, I had something to tell. I started working with the colors of the background and discover the “random()” command, 
where I was only partially in control of which color it created. I thought it would just pick a random color and it would stay that color until I reloaded the page, but instead
I created this epileptic background. It gave the canvas very high energy and I wanted to expand that energy to the rest of the canvas. I made a cutout of the kid in the picture’s
face in photoshop, saved is as an .png to see if p5.js worked with that format and implemented via a position randomizer. After this I left the code for a day or two and came back
re inspired. The high energy of the canvas in combination with the kid pictured made me think of the unhealthy lifestyle of children with excitability to unhealthy food. I wanted
to add major fastfood chains’ logos into the code. At first I tried to just add pictures, but it made the image more stale and it lost some of that energy I tried to convey. So I
started looking at how to implement flashing GIFs. This was a little harder, but my actions were very driven by my desire to create something and say something. After figuring out
how to add these moving pictures and adding them I was satisfied with my work.

After my first experience with coding I am enjoying it very much. I really love the graphical aspect of this way of using another language to create something visual. Language in
general have to possibility to create images in peoples’ heads, but these images are subjective constructs and they incomprehensible and indescribable to other people. But to have
a language that is both readable to someone else and able to convey an image to others is very fascinating. 

I think that my love for graphical design will be very explored and challenged over the course of this, well, course. As you have read in my description of my first experience with
p5.js most of the actions taken were driven by coincidences up until the very end of my process. I this kind of exploration of what I want to create and say is something I 
enjoy very much!
