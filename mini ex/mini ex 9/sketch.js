//the code is not commented, but that does not mean it has nothing to say
  //exept for that comment obviously - and this one - and another at the bottom
var loveYou;
  var sweetheart;
    var wholesomeWords;
      var holeInMyHeart;
        var comeBack;
          var pouringMyHeartOut;

function preload(){

loveYou = loadJSON("PleaseLoveMe.json");
loveLetters = loadFont('PIXEARG_.TTF');
iMissYou=loadImage("IMissYou.png");

}

function setup(){

createCanvas (windowWidth,windowHeight);

}

function draw() {

noLoop();

background(255);

image(iMissYou,0,0,1000,750);

fill(0,255,0);
  textFont(loveLetters);
    textSize(10);

      holeInMyHeart = ' ';

      wholesomeWords = ['Dear', random(loveYou.gentleGreeting),random(loveYou.lovelyNickname)];
        comeBack = join(wholesomeWords, holeInMyHeart);
          text(comeBack, 280, 220);

      pouringMyHeartOut = ['You are a', random(loveYou.compliments), random(loveYou.baby), 'A',
      random(loveYou.compliments), random(loveYou.baby), 'Your', random(loveYou.compliments),
      random(loveYou.bae), random(loveYou.Longing4U), 'my', random(loveYou.whyAreYouTheWayYouAre), random(loveYou.stupidIdoit),
      'You are a', random(loveYou.compliments), random(loveYou.baby)];
        iCannotLiveWithoutYou = join(pouringMyHeartOut, holeInMyHeart);
          text(iCannotLiveWithoutYou, 280, 260, 300);

      thisIsNotTheEnd = ['Yours', random(loveYou.doNotLeaveMe)];
        doNotGo = join(thisIsNotTheEnd, holeInMyHeart);
          text(doNotGo, 280, 320);

          text('A . U .', 280, 350);

tryAgain = createButton('START OVER');
  tryAgain.position(300, 450);
    tryAgain.size(240, 20);
      tryAgain.style('font-weight', 'bold');
        tryAgain.style('font-style', 'italic');
          tryAgain.style('border-radius', '7px');
            tryAgain.style('border-color', '#b79973');
              tryAgain.style('background', '#cfb994');
                tryAgain.style('font-family', 'Times New Roman');
                  tryAgain.mousePressed(canWePleaseGoBackToTheBeginning);
}

function canWePleaseGoBackToTheBeginning() {
  loop();
} // 69 <3
