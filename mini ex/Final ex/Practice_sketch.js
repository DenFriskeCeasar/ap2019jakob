var earth;
var batLevel;
let video;
let vWidth=32;
let vHeight = 24;
let hue = 200;
let brightness;
let pixValues = [];

navigator.getBattery().then(function(battery) {
  batLevel = battery.level;
  battery.onlevelchange = function() {
    batLevel = battery.level;
  };
});

function setup() {
  frameRate(15);
  createCanvas(windowWidth, windowHeight);
  colorMode(HSB, 360, 100, 100, 100);
  noStroke();
  pixelDensity(1);

video=createCapture(VIDEO);
video.size(vWidth, vHeight);
video.hide();

}

  function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
  }


  function getBrightness(){
  pixValues=[];
  brightness=0;

  video.loadPixels()
  for (let y = 0; y < vHeight; y++){
    for (let x = 0; x < vWidth; x++){
      let index=(x + y * vWidth) * 4;
      let r = video._pInst.pixels[index + 0];
      let g = video._pInst.pixels[index + 1];
      let b = video._pInst.pixels[index + 2];

      pixValues.push((r + g + b)/3);
    }
  }
    for(let i = 0; i < pixValues.length; i++){
      brightness += pixValues[i]
  }
    brightness = floor(brightness/pixValues.length);
  }

function draw() {

getBrightness();
if (brightness){
  background(hue,80,brightness);
}else{
  background(180,80,50)
}
    if (hue > 100) hue = 200;

  earthColor();

  push()
  fill(0)
  text('v.1',100,100)
  pop()

  //table
  fill(color="#885b3a");
  rect(0,height-100,width,130);

  //pot
  push();
  rectMode(CENTER);

  //bottom part of the pot
  fill(color="#b68960");
  ellipse(width/2,height-90,250,40);
  rect(width/2,height-115,250,50);

  //top part of the pot
  fill(color="#a9754e");
  ellipse(width/2,height-120,270,40);
  rect(width/2,height-130,270,25);
  ellipse(width/2,height-140,270,40);

  //earth in the pot
  fill(color="#4a2a1b");
  ellipse(width/2,height-140,250,30);
  fill(earth);
  ellipse(width/2,height-138,250,30);
  pop();
  }

function earthColor(){

  if (batLevel<0.3){
  earth = color="#debf9e"
  } else if (batLevel>=0.3 && batLevel<=0.7){
  earth = color="#6a4125"
  } else if (batLevel>0.7){
  earth = color="#291609"
  }


}
