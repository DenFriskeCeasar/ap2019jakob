var words
var weblinks
var button
var button2
var r

function preload(){


  words=[
      'Yellow teeth detected',
      'An increase in bodyfat detected',
      'Poor living conditions detected',
      'Detected several impurities on your skin',
      'A clothing piece not in line with current fashion trends detected',
      'Under average size breasts detected',
      'Uneven and shaggy hair detected',
      'Bad eyesight detected',
      'Inconsistent heartbeat detected',
      'Bad shave detected',
      'Unwealthy lifestyle detected',
      'Low cognitive Low processing abillities detected',
      'Disobedient children detected',
      'Old age detected',
      'Bad taste in music detected',
      'Detected a tear on the chin',
      'Loneliness detected',
      'Insect detected',

    ]

   weblinks=[
     'https://www.needymeds.org/dental-clinics',
     'https://www.anytimefitness.com/find-gym',
     'https://www.newhomesource.com/',
     'https://www.sephora.com/shop/skincare',
     'https://www.gucci.com/',
     'https://www.plasticsurgery.org/cosmetic-procedures',
     'https://www.thebarbershops.com/',
     'https://www.mayoclinic.org/tests-procedures/lasik-eye-surgery/about/pac-20384774',
     'https://www.bcbs.com/find-a-doctor',
     'https://gillette.co',
     'https://www.bankofamerica.com/',
     'https://www.education.com/',
     'https://adoption.com/',
     'https://www.rocketlawyer.com/form/last-will-and-testament.rl#/',
     'http://www.pinkfloyd.com/',
     'https://www.kleenex.com/en-us/',
     'https://tinder.com/',
     'https://www.orkin.com/pest-control',
   ]

}

function setup() {
createCanvas(windowWidth,windowHeight)
background(255)

textAlign(CENTER)

push()
fill(color="#444448");
textSize(16);
text('Please allow us to use your webcam and mic', width/2, windowHeight/2-20);
pop()

button = createButton('Press to improve your life');
button.style('width','300px')
button.position(windowWidth/2-150,windowHeight/2);
button.mousePressed(buffer);

var capture = createCapture();
capture.size(0,0);
capture.position(0,0);

}

function buffer() {

button.remove()

push()
noStroke()
fill(color="#ddddff")
rect(0,0,windowWidth,windowHeight)
pop()

push()
fill(color="#444448");
textSize(16);
text('Analyzing...', width/2, windowHeight/2+150);
pop()

throbber=createImg("8CLV.gif")
throbber.size(200,200)
throbber.position(width/2-100,windowHeight/2-100)

setTimeout(improveLife,10000)
}

function improveLife(){
throbber.size(0,0)

 r=floor(random(0,words.length))

  push()
  noStroke()
  fill("white")
  rect(0,0,windowWidth,windowHeight)
  pop()

  push()
  fill(color="#444448");
  textSize(16);
  text(words[r], width/2, windowHeight/2-20);
  pop()


buttontwo = createButton('Press to find a soultion for this problem');
buttontwo.style('width','300px')
buttontwo.position(windowWidth/2-150,windowHeight/2);
buttontwo.mousePressed(link);

}

function link(){
  window.open(weblinks[r])
}
