![Screenshot](fsg.PNG)
![Screenshot](hgfd.PNG)
![Screenshot](fghjg.PNG)

[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%204/index.html)

This project is called “Life Improver”

The first thing you see in this project is a screen which asks the user to allow the camera and the microphone to be used. Under this text is placed a button with the
words: “Press to improve your life”. When this button is pressed a buffer is shown with the word “analyzing” underneath. After 10 seconds new text and button is shown.
This time the texts is something with seems to be detected from the mic or camera criticizing something. If you press the button you are given a “solution” to this
problem, as it redirects you to a webpage, on which you can buy something corresponding to what problem is detected.

The code mostly consist of text() and button() syntaxes, but to have a new text and corresponding link was made with the help of arrays. I wanted the capture ALL 
function to actually detect these things “wrong” with people, but doing this would have been far too complicated and time-consuming at this stage of my programing
learning. The capture ALL in this code is merely included to have the browser ask for permission to use the camera and mic.

What I wanted to explore with this program is the limited and almost nonexistent information given to the user. When you give permission to the webpage, you do not
know what your information is going to be used on or what it is “analyzing”.  Your data could be used for anything. Another thing it does not tells you is why it 
chooses specific webpages as your solution. When the program redirects you to “Pinkfloyd.com”, when it tries to improve your taste in music, why have it chosen this 
specific band? Is it because it has a brand-deal with this band? Will the link change if another band pays more money to have their webpage featured?

The program also assumes that your life can be improved with money. All of the solutions given to problems are just ads. You have your data collected – not to improve
your life – but to be given ads. This is what using the internet feels like most of the time. You use something like facebook and what you like or what you write about 
is collected and you are given ads corresponding to your likes (unless you have ad-block). When a webpage becomes popular enough and needs to make money to keep its webpage 
going, the consumers change from users to products as their time can be filled with ads. 

I hate modern internet.
