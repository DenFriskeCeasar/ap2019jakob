![Screenshot](scre.PNG)

[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%208/index.html)

This work is called “no love letter”. 

It was made in collaboration with Maja Størum.

For this we wanted to create something in reference to the “Love Letter” algorithm created by Christopher Strachey. We ended up creating what would seem like a hate letter, but which does in fact have a deeper meaning.  This letter would function on the same sort of framework made by Christopher Strachey, but instead of finding the source code and changing it, we opted to emulate the functions of the letter using a JSON-file. The biggest refence is that when to code mentions “my” all of the generated words are some of the same words in Strachey’s code.

When you run the code, you see an old computer, with green text on the screen which looks like a hate letter. Then you should see a button on the screen, which says “start over”. Pressing this button generates a new hate letter. Simple. The code however shows a different story. Were you only seeing the program in function, you would not realize that there might be something else going on. 

We wanted to tell a story with the code as well and we thought about how we could do this a lot. We came up with the idea to have all of the functions, variables, pictures, refences to the JSON-file and arrays to be affectionate or heartbroken words and phrases. With this we wanted to create the narrative, that the person who made this code wanted to show anger the his/her ex with the letter, but deep down (deep down in the code) still loved her and was very heartbroken. 

With this we ended up saying something about what a computer makes out of the code and what a human would make out of the code. A human could very realistically detect what was going on, but the computer would have a very hard time to realize the same thing. The computer executes only, what it sees as arrays and functions, but it is the human creater and the human user, who can interoperate the deeper meaning. The value of language for humans and the possibility to interoperate things in many different ways is something that distinct us from most “thinking” machines.

Working collaborative is always fun and this code is equally mine and equally maja’s. Even though it can sometimes seem harder, when only one person can work on the code at a time, the end result justifies it for me. This is easily the best work I have helped create.

