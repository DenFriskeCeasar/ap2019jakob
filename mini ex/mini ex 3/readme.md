![Screenshot](winscreen.PNG)

[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%203/index.html)

This project is called: “Throwback Throbber Text”

It features a pop-up from something which looks like it was taken from windows 95. The pop-up appears to be loading a program called “dingus_pingus.exe”,
about which we know nothing. The only indication of process is the blue cubes, which are moving across the pop-up. After the first time it has reached the end – it
moves back to the start and a small text shows up just above the loading bar. This text is randomly chosen and is mostly nonsense.
I have always loved the windows 95 style. The simple shapes and the very contrasting colors. Working with these kinds of simple shapes can seem boring to others
and I get that, but the nostalgic value is just too great for me not to want to create something with this.These were my first thoughts going into this mini_ex. 
My second thoughts were: I don’t remember seeing any throbbers in windows 95 – there were some – but I could not remember them. The only thing I could really 
remember was the loading pop-up, which is what I tried to redesign into a throbber.

I created a similar looking pop-up using the rect() syntax and made blue squares appear, duplicate to the left and disappear using a for loop. I was at first
okay with this throbber, but I wanted to explore the entertainment value of a thobber as well. Every throbber I have come across in my entire life has just been
some shapes moving around simulating activity not giving any information on progress, just staling. How can you make something like this enjoyable to watch?
It made me think of my favourite loading screen of all time: the loading screen from Sims 2. Even thought it actually showed activity, it also featured funny 
things the program was saying while it was loading. Things it was clearly not doing like “Preparing a tasty grilled cheese sandwich”. 
 
I added an array to the code containing different text-lines, that are either silly, nonsense, the worst thing you would want a loading screen to say or 
just META-jokes about programming I wrote about 70 lines and found around 50 on the internet. 

The goal of this throbber is to either entertain the user while waiting or to frustrate the user. I can’t say which one of these it would achieve, it is up for the
user to decide. Personally I would rather be entertained than frustrated, and something like this entertains me.

